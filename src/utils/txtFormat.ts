/**
 * 检测txt文本格式，当前支持utf-8/gb2312
 * @param file
 * @param callback
 */
export function check_encoding(file, callback) {
  let fReader = new FileReader()
  fReader.readAsText(file)
  fReader.onload = (evt: any) => {
    let str = evt.target.result
    const sampleStr = str.slice(4, 4 + str.length / 2)
    if (sampleStr.indexOf('�') === -1) {
      callback('UTF-8')
    } else {
      callback('GB2312')
    }
  }
}
