import { createApp } from 'vue'
import './style.css'
import '@/assets/styles/element-ui-reset.scss'
import 'element-plus/theme-chalk/src/index.scss'
import router from './router/index'
import App from './App.vue'
import baiduAnalytics from 'vue-baidu-analytics'
import analytics from '@/utils/analytics'
import '@/utils/ads'

const app = createApp(App)
app.directive('analytics', analytics)
app.use(baiduAnalytics, {
  router: router,
  siteIdList: ['fac737ee660fb225fb75bc0b16349c01'],
  isDebug: false,
})
app.use(router)
app.mount('#app')
