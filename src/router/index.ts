import { createRouter, createWebHistory } from 'vue-router'

// 公共路由
export const constantRoutes = [
  {
    path: '/:catchAll(.*)',
    redirect: '/',
  },
  {
    path: '/',
    component: () => import('@/views/index.vue'),
  },
  {
    path: '/book/:id',
    component: () => import('@/views/book/index.vue'),
    name: 'book',
  },
  {
    path: '/admin',
    component: () => import('@/views/verify.vue'),
    name: 'admin',
  },
  {
    path: '/meili-manage',
    component: () => import('@/views/manage/index.vue'),
    name: 'meili-manage',
  },
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = []

const router = createRouter({
  history: createWebHistory(process.env.NODE_ENV === 'production' ? '/' : '/'),
  routes: constantRoutes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        top: 0,
      }
    }
  },
})

export default router
