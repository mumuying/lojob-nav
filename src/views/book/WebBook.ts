export class WebBook {
  /**
   * 唯一ID
   */
  id: String
  /**
   * 标题
   */
  title: String
  /**
   * 作者
   */
  author: String
  /**
   * 简介、提要
   */
  synopsis: String
  /**
   * 分类，主要用分类，如果有的网站只有标签，那会将标签合并到分类中使用
   */
  classify: String
  /**
   * 标签
   */
  label: String
  /**
   * 评分
   */
  appraise: number
  /**
   * 字数
   */
  wordCount: String
  /**
   * TXT大小
   */
  txtSize: String
  /**
   * 编辑状态
   */
  editStatus: String
  /**
   * 电子书在每个网站上的独有信息
   */
  webSites: any
}
export class WebSite {
  /**
   * 网站名称，使用简写，例如知轩藏书、起点中文网等等
   */
  name: String

  /**
   * 评分，本网站对电子书的评分
   */
  appraise: Number

  /**
   * 具体电子书对应的网址
   */
  url: String

  /**
   * 具体电子书对应的下载地址
   */
  downloadUrl: String

  /**
   * 校对（校对版、整理未校对精校版全本等等，不是很重要）
   */
  collation: String

  /**
   * 权重，用于计算评分
   */
  weight: Number
}
