import request from '@/utils/request'

// 上传电子书信息
export function uploadBook(books: any, keyStr: String) {
    return request({
        url: `/book/upload/${keyStr}`,
        method: 'post',
        data: books
    })
}

// 获取电子书信息
export function getBook(id: String) {
    return request({
        url: `/book/one/${id}`,
        method: 'get'
    })
}

// 获取电子书信息列表
export function listBooks(offset: Number, limit: Number) {
    return request({
        url: '/book/list',
        method: 'get',
        params: {
            offset,
            limit
        }
    })
}

// 搜索电子书信息列表
export function searchBooks(q: String, classify: String, offset: Number, limit: Number) {
    return request({
        url: '/book/search',
        method: 'get',
        params: {
            q,
            classify,
            offset,
            limit
        }
    })
}

// 推荐电子书信息列表
export function recommendBooks(classify: String, offset: Number, limit: Number) {
    return request({
        url: '/book/recommend',
        method: 'get',
        params: {
            classify,
            offset,
            limit
        }
    })
}

// 删除一本电子书
export function deleteDocument(keyStr: String, id: String) {
    return request({
        url: `/book/delete/${keyStr}/${id}`,
        method: 'Delete'
    })
}

// 验证密钥
export function verify(keyStr: String) {
    return request({
        url: '/book/verify',
        method: 'get',
        params: {
            keyStr
        }
    })
}

// 获取电子书信息列表
export function getStats(keyStr: String) {
    return request({
        url: `/book/stats/${keyStr}`,
        method: 'get'
    })
}

// 获取电子书信息列表
export function clearIndex(keyStr: String, uid: String) {
    return request({
        url: `/book/clear/${keyStr}/${uid}`,
        method: 'Delete'
    })
}
