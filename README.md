# 小机灵鬼阅读器电子书仓库-前端

使用 vue3+vite 开发，支持模糊查询、按照评分排序，提供电子书导航。

## 介绍

**仅作为演示和学习使用。**

## 网站演示

- [小机灵鬼阅读器电子书仓库](https://book.gremlins-book.com)：https://book.gremlins-book.com
- [小机灵鬼阅读器](https://www.gremlins-book.com)：https://www.gremlins-book.com
- [小机灵鬼阅读器博客](https://blog.gremlins-book.com)：https://blog.gremlins-book.com

## 爬虫仓库

仓库地址：https://gitee.com/mumuying/lojob-spider.git

## 使用教程

项目启动前请启动后端服务。具体可参考后端服务仓库。

```shell
# 1.下载源码
git clone git clone https://gitee.com/mumuying/lojob-nav.git
# 2.安装依赖
yarn
# 3.启动
vite
```

## 网站功能

- 小说查询
- 评分推荐
- 小说管理，包括批量新增爬虫数据（含合并）、删除或批量数据等，管理功能在演示网站中需要验证后使用
- 小说详情，基于爬虫数据

## 预览

![小机灵鬼阅读器电子书库页面](ebook.png '小机灵鬼阅读器电子书库页面')
